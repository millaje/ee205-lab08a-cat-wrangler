///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file queue.hpp
/// @version 1.0
///
/// A generic Queue collection class.
///
/// @author Jeraldine Milla <millaje@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   9 April 2021
///////////////////////////////////////////////////////////////////////////////

#include "node.hpp"
#include "list.hpp"

#pragma once

class Queue {
protected:
	DoubleLinkedList list = DoubleLinkedList();

public:
	   inline const bool empty() const { return list.empty(); };
	inline const unsigned int size() const{ return list.size();};
   inline void push_front(Node* newNode){ return list.push_front(newNode);};
   inline Node* pop_back(){ return list.pop_back();};
   inline Node* get_first() const{ return list.get_first();};
   inline Node* get_next(Node* currentNode) const{ return list.get_next(currentNode);};
   // Using the above as an example, bring over the following methods from list:
	// Add size()
	// Add push_front()
	// Add pop_back()
	// Add get_first()
	// Add get_next()

}; // class Queue
