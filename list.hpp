///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.hpp
/// @version 1.0
///
/// Exports data 
///
/// @author Jeraldine Milla <millaje@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   7 Apr 2021 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include <string>
//#include <cstdlib>
#include "node.hpp"


using namespace std;

   class DoubleLinkedList{
      protected:
         Node* head = nullptr;
         Node* tail = nullptr;
      private:
         unsigned int nodeCount =0;

      public:
         const bool empty() const;
         void push_front(Node* newNode);
         Node* pop_front();
         Node* get_first() const;
         Node* get_next(const Node* currentNode) const;
         inline unsigned int size() const{return nodeCount;};
         void push_back(Node* newNode);
         Node* pop_back();
         Node* get_last() const;
         Node* get_prev(const Node* currentNode) const;
         void swap(Node* node1, Node* node2);
         const bool isSorted() const;
         void insertionSort();
         void insert_before (Node* currentNode, Node* newNode);
         void insert_after (Node* currentNode, Node* newNode);
         
         //extras
         bool validate() const;
         const bool isIn(Node* node) const;
         bool isBefore(Node* a, Node* b);
         bool isAfter(Node* a, Node* b);
   };





