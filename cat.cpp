///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file cat.cpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author Jeraldine Milla <millaje@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   7 Apr 2021
///////////////////////////////////////////////////////////////////////////////

#include <cassert>
#include <random>

#define CAT_NAMES_FILE "names.txt"

#include "cat.hpp"

using namespace std;


Cat::Cat( const std::string newName, const float newWeight, const int newDaysAlive ) {
	setName( newName );
	setWeight( newWeight );
	setDaysAlive( newDaysAlive );
}

Cat::~Cat() {
	// We just need the virtual destructor
}


void Cat::setName( const std::string newName ) {
	assert( newName.length() > 0 );

	name = newName;
}


void Cat::setWeight( const float newWeight ) {
	assert( newWeight >= 0 );
	assert( newWeight <= MAX_CAT_WEIGHT );

	weight = newWeight;
}


void Cat::setDaysAlive( const int newDaysAlive ) {
	assert( newDaysAlive >= 0 );
	assert( newDaysAlive <= MAX_CAT_AGE );

	daysAlive = newDaysAlive;
}


std::string Cat::getInfo() const {
	return "Cat Name = [" + name + "]"
	     + "    Weight = [" + std::to_string( weight ) + "]"
	     + "    Days Alive = [" + std::to_string( daysAlive ) + "]";
}


// Override Node's > operator and sort based on Cat's members
bool Cat::operator>(const Node& rightSide) {
	// this is the leftSide of the operator, so compare:
	// leftSide > rightSide

	return compareByName( this, &(Cat&)rightSide );
}


bool Cat::compareByName ( const Cat* node1, const Cat* node2 ) {
	if( node1->name > node2->name )
		return true;
	return false;
}


bool Cat::compareByWeight ( const Cat* node1, const Cat* node2 ) {
	if( node1->weight > node2->weight )
		return true;
	return false;
}


bool Cat::compareByDaysAlive ( const Cat* node1, const Cat* node2 ) {
	if( node1->daysAlive > node2->daysAlive )
		return true;
	return false;
}


std::vector<std::string> Cat::names;


void Cat::initNames() {
	if( names.empty() ) {
		cout << "Loading... ";

		ifstream file( CAT_NAMES_FILE );
		string line;
		while (getline(file, line)) names.push_back(line);

		cout << to_string( names.size() ) << " names." << endl;
	}
}


random_device randomDevice;        // Seed with a real random value, if available
mt19937_64 RNG( randomDevice() );  // Define a modern Random Number Generator
uniform_int_distribution<>  nameRNG   (0   ,10000              );
uniform_real_distribution<> weightRNG (0.1 ,Cat::MAX_CAT_WEIGHT);
uniform_int_distribution<>  ageRNG    (0   ,Cat::MAX_CAT_AGE   );


Cat* Cat::makeCat() {
	return new Cat( names[nameRNG( RNG ) % names.size()], (float) weightRNG( RNG ), ageRNG( RNG ) );
}



