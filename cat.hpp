///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file cat.hpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author Jeraldine Milla <millaje@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   7 Apr 2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
#include <vector>
#include <fstream>
#include <iostream>

#include "node.hpp"

class Cat : public Node {
private:  /// Member variables
	std::string name;
	float       weight;
	int         daysAlive;

private:  /// Static variables
	static std::vector<std::string> names;

public:
	/// Constructors
	Cat( const std::string newName, const float newWeight, const int newDaysAlive );
	virtual ~Cat();

	/// Constants
	static constexpr const int   MAX_CAT_AGE = 365 * 30;
	static constexpr const float MAX_CAT_WEIGHT = 30;

	/// Operator overrides
	virtual bool operator>(const Node& r);

	/// Setters
	void setName( const std::string newName );
	void setWeight( const float newWeight );
	void setDaysAlive( const int newDaysAlive );

	/// Methods
	virtual std::string getInfo() const;

	/// Static methods
	static void initNames();
	static Cat* makeCat();

	static bool compareByName ( const Cat* node1, const Cat* node2 );
	static bool compareByWeight ( const Cat* node1, const Cat* node2 );
	static bool compareByDaysAlive ( const Cat* node1, const Cat* node2 );
};
