///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Jeraldine Milla <millaje@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler  - EE 205 - Spr 2021
/// @date   7 Apr 2021 
///////////////////////////////////////////////////////////////////////////////

#include <cassert>
#include <iostream>
#include <cstdlib>
#include <string>
#include "list.hpp"
//#include <algorithm>
using namespace std;

   const bool DoubleLinkedList::empty() const{
      //return head == nullptr;
      if(head == nullptr)
         return true;
      else
         return false;
   }
  /* unsigned int DoubleLinkedList::size() const{
      return nodeCount;
   }
   */
   void DoubleLinkedList::push_front(Node* newNode){
      if(newNode == nullptr)
         return;
      
      if( head == nullptr){
         newNode-> next = nullptr;
         newNode-> prev = nullptr;
         head = newNode;
         tail = newNode;
      
      }else{
         newNode->next = head;
         newNode-> prev = nullptr;
         head-> prev = newNode;
         head = newNode;
      }
      nodeCount++;
      validate();
   }  

   Node* DoubleLinkedList::pop_front(){
      if(head==nullptr)
         return nullptr;
      if(head == tail){
         Node* pop = head;
         head = nullptr;
         tail = nullptr;
         nodeCount--;
         validate();
         return pop;
      }else{
         Node* pop = head;
         head = head->next;
         head -> prev =nullptr;
         pop -> next = nullptr;
         nodeCount--;
         validate();
         return pop;
      }
   }
   Node* DoubleLinkedList::get_first() const{
      if( head== nullptr)
         return nullptr;
      else
         return head;
   }

    Node* DoubleLinkedList::get_next(const Node* currentNode) const{
       if (currentNode ->next == nullptr)
          return nullptr;
       else
          return currentNode->next;
    }
   

   void DoubleLinkedList::push_back(Node* newNode){
      if(newNode == nullptr)
         return;

      if( tail == nullptr){
         newNode-> next = nullptr;
         newNode-> prev = nullptr;
         head = newNode;
         tail = newNode;

      }else{
         newNode->next = nullptr;
         newNode-> prev = tail;
         tail-> next = newNode;
         tail = newNode;
      }
      nodeCount++;
      validate();
      return;
   }

   Node* DoubleLinkedList::pop_back(){
      if(head==nullptr)
         return nullptr;
      if(head== tail){
         Node* pop = tail;
         head = nullptr;
         tail = nullptr;
         nodeCount--;
         validate();
         return pop;
      }else{
         Node* pop = tail;
         tail = tail->prev;
         tail -> next =nullptr;
         pop->prev = nullptr;
         nodeCount--;
         validate();
         return pop;
      }
   }
   Node* DoubleLinkedList::get_last() const{
      if( tail == nullptr)
         return nullptr;
      else
         return tail;
   }

   Node* DoubleLinkedList::get_prev(const Node* currentNode) const{
       if (currentNode ->prev == nullptr)
          return nullptr;
       else
          return currentNode->prev;
    }
   
  
   const bool DoubleLinkedList::isIn(Node* node) const{
      Node* currentNode =head;

      while(currentNode != nullptr){
         if(node == currentNode)
            return true;

         currentNode = currentNode->next;
      }
      return false;
   }

   bool DoubleLinkedList::isBefore( Node* nodeA, Node* nodeB){
      Node* is = head;

      while (is != nullptr){
         if(nodeA == is){
            return true;
         }else if (nodeB == is){
            return false;
         }
         is = is->next;
      }
      return false;
   }

      void DoubleLinkedList::insert_before(Node* currentNode, Node* newNode){
      if( currentNode == nullptr && head == nullptr){
         push_front(newNode);
         return;
      }

      if((currentNode != nullptr && head == nullptr) || (currentNode == nullptr && head != nullptr)){
         assert(false);
      }

      assert(currentNode != nullptr && head != nullptr);
      assert(isIn(currentNode));
      assert(!isIn(newNode));
      
      if (head != currentNode){
         newNode->prev = currentNode->prev;
         currentNode->prev->next = newNode;
         currentNode->prev = newNode;
         newNode->next = currentNode;
         nodeCount++;
      }else{
         push_back(newNode);
      }
      validate();
   }

   void DoubleLinkedList::insert_after(Node* currentNode, Node* newNode){
      if( currentNode == nullptr && head == nullptr){
         push_front(newNode);
         return;
      }

      if((currentNode != nullptr && head == nullptr) || (currentNode == nullptr && head != nullptr)){
         assert(false);
      }
      
      assert(currentNode != nullptr && head != nullptr);
      assert(isIn(currentNode));
      assert(!isIn(newNode));

      if (tail != currentNode){
         newNode->next = currentNode->next;
         currentNode->next = newNode;
         newNode->prev = currentNode;
         newNode->next->prev = newNode;
         nodeCount++;
      }else{
         push_back(newNode);
      }
      validate();
   }

   void DoubleLinkedList::swap(Node* node1, Node* node2){
      assert(!empty());
      assert(node1 != nullptr && node2 != nullptr);
      assert( isIn(node1) &&  isIn(node2));
      
      if(node1 == node2){
         return;
      }
      
      if(!isBefore(node1, node2)){
         Node* before = node1;
         node1 = node2;
         node2 = before;
      }

      Node* aLeft_node = node1->prev;
      Node* aRight_node = node1->next;
      Node* bLeft_node = node2->prev;
      Node* bRight_node = node2->next;

      bool isAdjacent = (aRight_node == node2);

      if(!isAdjacent){
         node1->prev = node2->prev;
      }else{
         node1->prev = node2;
      }

      if(node1 != head){
         aLeft_node->next = node2;
         node2->prev = aLeft_node;
      }else{
         node2->prev = nullptr;
         head = node2;
      }

      if(!isAdjacent){
         node2->next = aRight_node;
      }else{
         node2->next = node1;
      }

      if(node2 != tail){
         bRight_node->prev = node1;
         node1->next = bRight_node;
      }else{
         node1->next = nullptr;
         tail = node1;
      }

      if(!isAdjacent){
         aRight_node->prev = node2;
         bLeft_node ->next = node1;
      }
      validate();

   }
                   
//helper

   bool DoubleLinkedList::validate() const{
      if(head== nullptr){
         assert (tail == nullptr);
         assert(empty());
         assert(size()==0);
      }else{
         assert(tail != nullptr);
         assert(!empty());
         assert(size()>0);
      }

      if(tail == nullptr){
         assert(head== nullptr);
         assert(empty());
         assert(size()==0);
      }else{
         assert(head != nullptr);
         assert(!empty());
         assert(size()>0);
      }

      if(head != nullptr && head == tail){
         assert(size()==1);
      }

      unsigned int count = 0;
      Node* currentNode = head;
      while (currentNode != nullptr){
         count++;

         if(currentNode ->next != nullptr)
            assert(currentNode ->next->prev == currentNode);
         currentNode = currentNode->next;
      }

      assert(size() == count);

      unsigned int tnouc = 0;
      currentNode = tail;
      while( currentNode != nullptr){
         tnouc++;

         if(currentNode->prev != nullptr)
            assert(currentNode->prev->next == currentNode);
         currentNode = currentNode->prev;
      }
      return true;
   }

   const bool DoubleLinkedList::isSorted() const{
      if(nodeCount <=1)
         return true;

      for( Node* a = head; a->next !=nullptr; a = a->next){
         if(*a > *a->next)
            return false;
      }
      return true;
   }

    


   void DoubleLinkedList::insertionSort(){
      Node* currentNode = head;
      Node* newNode = nullptr;
      Node* someNode = nullptr;

      while(currentNode != nullptr){
         someNode = currentNode;
         newNode = currentNode->next;

         while(newNode != nullptr){
            if(*someNode > *newNode)
               someNode = newNode;

            newNode = newNode->next;
         }

         swap(currentNode, someNode);

         currentNode = someNode->next;
      }
   }




