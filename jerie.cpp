///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file jerie.cpp
/// @version 1.0
///
/// My own unit test
///
/// @author Jeraldine Milla <millaje@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   6 Apr 2021
///////////////////////////////////////////////////////////////////////////////


#include <iostream>
#include <cassert>
#include "node.hpp"
#include "list.hpp"
#include "cat.hpp"
using namespace std;

int main(){

/*   cout << "Hello World" <<endl;
   Node node; // Instantiate a node
   DoubleLinkedList list;  // Instantiate a DoubleLinkedList
   
   cout<<"The list is empty: "<< boolalpha << list.empty() << endl;
   

   list.push_front(new Node());
   cout <<"new Node added :)"<< endl;
   cout <<"eeeemppty??"<<boolalpha <<list.empty()<<endl;
   list.push_front(new Node());
   cout <<"anotha new Node added :)"<< endl;
   cout<<"size: " << list.size()<<endl;

   list.pop_front();
   cout<<"node removed"<<endl;
   cout<<"size: "<<list.size()<<endl;

   cout<<"uno node: "<< list.get_first()<<endl;
   list.push_front(new Node());
   list.push_front(new Node());
   list.push_front(new Node());
   cout<<"anotha new Node added :)... new size: " <<list.size()<<endl;

*/
   //Actual tester
   cout << "Cat Wrangler Unit Tests" << endl;

   Cat::initNames();
   cout<< "End--initNames" <<endl;

   DoubleLinkedList list = DoubleLinkedList();
   cout<< "Initiated--DoubleLinkedList" <<endl;

   assert( list.validate() );
   
   cout<< "Test--push_front Function" <<endl;
   for( int i = 0 ; i < 100 ; i++ ) {
      
      list.push_front( Cat::makeCat() );
      
      assert( list.validate() );
   }
   
   cout<< "Test--pop_front Function" <<endl;
   for( int i = 0 ; i < 100 ; i++ ) {

      list.pop_front();

      assert( list.validate() );
   }

   cout<< "Test--push_back Function" <<endl;
   for( int i = 0 ; i < 100 ; i++ ) {

      list.push_back( Cat::makeCat() );

      assert( list.validate() );
   }
   
   cout<< "Test--pop_back Function" <<endl;
   for( int i = 0 ; i < 100 ; i++ ) {

      list.pop_back();

      assert( list.validate() );
      }

   // Test insert_after
   cout << "Test insert_after" << endl;

   Cat* cat1 = Cat::makeCat();
   Cat* cat2 = Cat::makeCat();
   Cat* cat3 = Cat::makeCat();
//   Cat* cat4 = Cat::makeCat();
//   Cat* cat5 = Cat::makeCat();

   assert( list.validate() );
   assert( list.get_first() == nullptr );
   assert( list.get_last() == nullptr );

   list.insert_after( nullptr, cat1 );
   assert( list.isIn( cat1 ));
   assert( !list.isIn( cat2 ));

   assert( list.validate() );
   list.insert_after( cat1, cat2 );
   assert( list.isIn( cat2 ));

   list.pop_back();
   list.pop_back();


   // Test insert_before
   cout << "Test insert_before" << endl;

   assert( !list.isIn( cat1 ));
   assert( !list.isIn( cat2 ));
   list.insert_before( nullptr, cat1 );

   assert( list.validate() );
   list.insert_before( cat1, cat2 );
   assert( list.isIn( cat2 ));

   list.pop_back();
   list.pop_back();
    assert( list.validate() );
   assert( list.get_first() == nullptr );
   assert( list.get_last() == nullptr );


   // Test swap function
   cout << "Test swap" << endl;

   // One item in list
   cout<< "Test-- 1 Item" <<endl;
   list.push_front( cat1 );
   list.swap( cat1, cat1 );
   assert( list.validate() );

   // Two items in list
   cout<< "Test-- 2 Items" <<endl;
   list.push_back( cat2 );
   assert( list.get_first() == cat1 );
   assert( list.get_last() == cat2 );
   // list.dump();
   cout<< "Test-- SWAP 1-2 to 2-1" <<endl;
   list.swap( cat1, cat2 );
   // list.dump();
   assert( list.validate() );
   assert( list.get_first() == cat2 );
   assert( list.get_last() == cat1 );
   cout<< "Test-- SWAP back" <<endl;
   list.swap( list.get_first(), list.get_last() );
   assert( list.get_first() == cat1 );
   assert( list.get_last() == cat2 );

   // Three items in list
   cout<< "Test-- 3 Items" <<endl;
   cout<< "Test-- SWAP 1-2-3 to 2-1-3" <<endl;
   list.push_back( cat3 );   // 1 2 3
   list.swap( list.get_first(), list.get_next( list.get_first() )) ;  // 2 1 3
   assert( list.validate() );
   cout<< "Test-- SWAP 2-1-3 to 3-1-2" <<endl;
   list.swap( list.get_first(), list.get_last() );  // 3 1 2
   assert( list.validate() );
   cout<< "Test-- SWAP 3-1-2 to 3-2-1" <<endl;
   list.swap( list.get_next( list.get_first() ), list.get_last() );  // 3 2 1
   assert( list.validate() );
   cout<< "Test-- SWAP 3-2-1 to 1-2-3" <<endl;
   list.swap( list.get_first(), list.get_last() );  // 1 2 3
   assert( list.validate() );
   assert( list.get_first() == cat1 );
   assert( list.get_next( list.get_first()) == cat2 );
   assert( list.get_last() == cat3 );
  /* // Four items in list
   list.push_back( cat4 );  // 1 2 3 4
   list.swap( list.get_first(), list.get_next( list.get_first() )) ;  // 2 1 3 4
   assert( list.validate() );
   list.swap( list.get_prev( list.get_last() ), list.get_last() );    // 2 1 4 3
   assert( list.validate() );
   list.swap( list.get_first(), list.get_last() );                    // 3 1 4 2
   assert( list.validate() );
   list.swap( list.get_next( list.get_first() ), list.get_prev( list.get_last() )); // 3 4 1 2
   assert( list.validate() );
   assert( list.get_first() == cat3 );
   assert( list.get_next( list.get_first()) == cat4 );
   assert( list.get_prev( list.get_last()) == cat1 );
   assert( list.get_last() == cat2 );

   // Five items in list
   list.push_back( cat5 );  // 3 4 1 2 5
   list.swap( list.get_first(), list.get_next( list.get_first() )) ;  // 4 3 1 2 5
   assert( list.validate() );
   list.swap( list.get_next( list.get_first() ), list.get_next( list.get_next( list.get_first() )) ); // 4 1 3 2 5
   assert( list.validate() );
   list.swap( list.get_next( list.get_next( list.get_first() )), list.get_prev( list.get_last() ) );  // 4 1 2 3 5
   assert( list.validate() );
   list.swap( list.get_prev( list.get_last() ), list.get_last() );    // 4 1 2 5 3
   assert( list.validate() );
   assert( list.get_first() == cat4 );
   assert( list.get_next( list.get_first()) == cat1 );
   assert( list.get_next( list.get_next( list.get_first() )) == cat2 );
   assert( list.get_prev( list.get_last()) == cat5 );
   assert( list.get_last() == cat3 );
*/
   cout << "Test Insertion Sort" << endl;
   list.pop_front();
   list.pop_front();
   list.pop_front();
  // list.pop_front();
  // list.pop_front();

   // Empty list
   cout<< "Test-- Insertion Sort" <<endl;
   list.insertionSort();
   assert( list.isSorted() );

   // One item in list
   cout<< "Test-- 1 Item List" <<endl;
   list.push_front( cat1 );
   assert( list.isSorted() );
   list.insertionSort();
   assert( list.isSorted() );

   // Two items in list
   cout<< "Test-- 2 Item List" <<endl;
   list.push_front( cat2 );
   list.insertionSort();
   assert( list.isSorted() );
   // Three items in list
   cout<< "Test-- 3 Item List" <<endl;
   list.push_front( cat3 );
   list.insertionSort();
   assert( list.isSorted() );
/*
   // Four items in list
   list.push_front( cat4 );
   list.insertionSort();
   assert( list.isSorted() );

   // Five items in list
   list.push_front( cat5 );
   list.insertionSort();
   assert( list.isSorted() );
*/
   for( int i = 0 ; i < 100 ; i++ ) {  // Outer loop
      DoubleLinkedList dll = DoubleLinkedList();
      for( int j = 0 ; j < 1000 ; j++ ) {
         dll.push_front( Cat::makeCat() );
      }
      assert( dll.validate() );
      assert( dll.size() == 1000 );
      assert( !dll.isSorted() );

      list.insertionSort();
      assert( list.isSorted() );

      for( int j = 0 ; j < 1000 ; j++ ) {
         delete (Cat*) dll.pop_front();
      }

      cout << ".";
      cout << std::flush;
   }
   cout << endl;

   // list.dump();
}






